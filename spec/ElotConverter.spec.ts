import {ElotConverter} from '../src/index';

describe('ElotConverter', () => {

    it('should convert greek given names', () => {
        expect(ElotConverter.convert('ΓΕΩΡΓΙΟΣ')).toBe('GEORGIOS');
        expect(ElotConverter.convert('Παρασκευάς')).toBe('Paraskevas');
        expect(ElotConverter.convert('Ελένη-Θεοδώρα')).toBe('Eleni-Theodora');
        expect(ElotConverter.convert('Maria')).toBe('Maria');
        expect(ElotConverter.convert('Αγλαΐα')).toBe('Aglaia');
    });

    it('should convert greek family names', () => {
        expect(ElotConverter.convert('Ιωαννίδης')).toBe('Ioannidis');
        expect(ElotConverter.convert('Μπαρούτης')).toBe('Baroutis');
        expect(ElotConverter.convert('Λαμπροπουλου')).toBe('Lampropoulou');
        expect(ElotConverter.convert('Οικονομόπουλος')).toBe('Oikonomopoulos');
    });

    it('should handle null or undefined', () => {
        expect(ElotConverter.convert(null as any)).toBe(null as any);
        expect(ElotConverter.convert(undefined as any)).toBe(undefined as any);
    });

});