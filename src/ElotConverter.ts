
const GREEK_CAPS = stringToSet('ΑΆΒΓΔΕΈΖΗΉΘΙΊΪΚΛΜΝΞΟΌΠΡΣΤΥΎΫΦΧΨΩΏ');

const GR_LETTERS = 'αάβγδεέζηήθιίϊΐκλμνξοόπρσςτυύϋΰφχψωώ';
const EN_LETTERS = 'aavgdeezii.iiiiklmnxooprsstyyyyf..oo';

function fixCase(text: string, mirror: string): string {
    if (GREEK_CAPS[mirror.charAt(0)]) {
        if (mirror.length === 1 || GREEK_CAPS[mirror.charAt(1)]) {
            return text.toUpperCase();
        } else {
            return text.charAt(0).toUpperCase() + text.substr(1);
        }
    } else {
        return text;
    }
}

function stringToSet(s: string): any {
    const o = {};
    for (let i = 0; i < s.length; i++) {
        o[s.charAt(i)] = 1;
    }
    return o;
}
/**
 * Elot 743 converter
 */
class ElotConverter {

    private static _replacements: any[];

    static get replacements(): any[] {
        if (ElotConverter._replacements != null) {
            return ElotConverter._replacements;
        }
        const replacements = [
            { greek: 'αι', greeklish: 'ai' },
            { greek: 'αί', greeklish: 'ai' },
            { greek: 'οι', greeklish: 'oi' },
            { greek: 'οί', greeklish: 'oi' },
            { greek: 'ου', greeklish: 'ou' },
            { greek: 'ού', greeklish: 'ou' },
            { greek: 'ει', greeklish: 'ei' },
            { greek: 'εί', greeklish: 'ei' },
            { greek: 'αυ', fivi: 1 },
            { greek: 'αύ', fivi: 1 },
            { greek: 'ευ', fivi: 1 },
            { greek: 'εύ', fivi: 1 },
            { greek: 'ηυ', fivi: 1 },
            { greek: 'ηύ', fivi: 1 },
            { greek: 'ντ', greeklish: 'nt' },
            { greek: 'μπ', bi: 1 },
            { greek: 'τσ', greeklish: 'ts' },
            { greek: 'τς', greeklish: 'ts' },
            { greek: 'τζ', greeklish: 'tz' },
            { greek: 'γγ', greeklish: 'ng' },
            { greek: 'γκ', greeklish: 'gk' },
            { greek: 'γχ', greeklish: 'nch' },
            { greek: 'γξ', greeklish: 'nx' },
            { greek: 'θ', greeklish: 'th' },
            { greek: 'χ', greeklish: 'ch' },
            { greek: 'ψ', greeklish: 'ps' }
        ];
        // remove extraneus array element
        if (!replacements[replacements.length - 1]) replacements.pop();
        // enchance replacements
        let replacement: any;
        for (replacement of replacements) {
            replacements[replacement.greek] = replacement;
        }
        // append single letter replacements
        for (let i = 0; i < GR_LETTERS.length; i++) {
            if (!replacements[GR_LETTERS.charAt(i)]) {
                replacements.push({ greek: GR_LETTERS.charAt(i), greeklish: EN_LETTERS.charAt(i) });
            }
        }
        for (const replacement1 of replacements) {
            replacements[replacement1.greek] = replacement1;
        }
        ElotConverter._replacements = replacements;
        return replacements;
    }

    /**
     * Converts a greek given name or family name
     * @param {string} text
     * @returns
     */
    static convert(text: string) {
        if (text == null) {
            return text;
        }
        // enchance replacements, build expression
        const expression = [];
        const replacements = ElotConverter.replacements;
        for (let i = 0; i < replacements.length; i++) {
            expression[i] = replacements[i].greek;
        }
        const expressionRegEx = new RegExp(expression.join('|'), 'gi');
        // replace greek with greeklsh
        const greekSet = stringToSet(GR_LETTERS);
        const viSet = stringToSet('άαβγδέεζήηλίιmμνόορώω');
        text = text.replace(expressionRegEx, (substr, index) => {
            const replacement1 = replacements[substr.toLowerCase()];
            if (replacement1.bi) {
                const bi = (greekSet[text.charAt(index - 1).toLowerCase()] && greekSet[text.charAt(index + 2).toLowerCase()]) ? 'mp' : 'b';
                return fixCase(bi, substr);
            } else if (replacement1.fivi) {
                const c1 = replacements[substr.charAt(0).toLowerCase()].greeklish;
                const c2 = viSet[text.charAt(index + 2).toLowerCase()] ? 'v' : 'f';
                return fixCase(c1 + c2, substr)
            } else {
                return fixCase(replacement1.greeklish, substr + text.charAt(index + substr.length));
            }
        });
        return text;
    }

}

export {
    ElotConverter
}